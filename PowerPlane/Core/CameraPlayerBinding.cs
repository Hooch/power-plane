﻿using System;
using PowerPlane.Entities;
using OpenTK;

namespace PowerPlane.Core
{
	public class CameraPlayerBinding
	{
		#region Fields

		/// <summary>
		/// Camra that is being bind.
		/// </summary>
		private Camera camera;

		/// <summary>
		/// Player to bind camera to.
		/// </summary>
		private Player player;

		/// <summary>
		/// Relative home position of camra to player.
		/// </summary>
		private Vector3 cameraPointToPlayer = new Vector3(0.0f, 1.5f, -2.0f);

		/// <summary>
		/// Relative camera target to player.
		/// </summary>
		private Vector3 targetToPlayer = new Vector3(0.0f, 0.0f, 10.0f);

		#endregion Fields

		#region Constructors

		/// <summary>
		/// Creates CameraPlayerBinding with default parameters.
		/// </summary>
		/// <param name="camera">Camera to manage.</param>
		/// <param name="player">Player to bind to.</param>
		public CameraPlayerBinding(Camera camera, Player player)
		{
			this.player = player;
			this.camera = camera;
		}

		#endregion Constructors

		#region Methods

		/// <summary>
		/// Update state of bindng and move camera.
		/// </summary>
		/// <param name="dt">Update time.</param>
		public void UpdateCamera(TimeSpan dt)
		{
			camera.Position = cameraPointToPlayer;
			camera.Position.X += player.Position.X * 0.9f;
			camera.LookAtTarget = player.Position + targetToPlayer;
		}

		#endregion Methods
	}
}
