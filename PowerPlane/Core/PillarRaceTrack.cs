﻿using PowerPlane.Entities;
using PowerPlane.Rendering.Models;
using System;
using System.Collections.Generic;
using OpenTK;
using PowerPlane.Managers;

namespace PowerPlane.Core
{
	internal class PillarRaceTrack
	{
		#region Fields

		/// <summary>
		/// Volume for drawing track under pillars.
		/// </summary>
		private Volume track;

		/// <summary>
		/// RNG used for spawning pillars at random place and time.
		/// </summary>
		private Random random;

		/// <summary>
		/// Time till next row apperas. Calculated on creation time of previous row.
		/// </summary>
		private double tillNextRowSpanSeconds;

		/// <summary>
		/// Indicates if there was collision in previous simulation step.
		/// </summary>
		private bool wasCollision;

		#endregion Fields

		#region Constructors

		/// <summary>
		/// Creates new pillar race track.
		/// </summary>
		/// <param name="width">Width of track</param>
		/// <param name="length">Length of track</param>
		/// <param name="speed">Speed of pillars in units/s</param>
		/// <param name="minTime">Minium time between fillar rows in seconds.</param>
		/// <param name="maxTime">Maximum time between fillar rows in seconds.</param>
		/// <param name="minInRow">Minium number of fillars in row.</param>
		/// <param name="maxInRow">Maximum number of fillars in row.</param>
		/// <param name="modelsManager">Models manager for given level that is not locket yet.</param>
		public PillarRaceTrack(float width, float length, float speed, float minTime, float maxTime, int minInRow, int maxInRow, ModelsManager modelsManager)
		{
			Width = width;
			SpawnPoint = length;
			Speed = speed;
			MinRespawnTime = minTime;
			MaxRespawnTime = maxTime;
			MaxInRow = maxInRow;
			MinInRow = minInRow;

			int maxPillarsCount = (int)Math.Ceiling(((length / speed) / minTime) * maxInRow);
			tillNextRowSpanSeconds = 0;
			random = new Random();

			Pillars = new Queue<Pillar>(maxPillarsCount);

			for (int i = 0; i < maxPillarsCount; i++)
			{
				Pillars.Enqueue(new Pillar { Position = new Vector3(-100f) });
			}

			foreach (var pillar in Pillars)
			{
				modelsManager.AddEntity(pillar);
			}

			track = new Cuboid(new Vector3(width, 0.1f, length + 10), new Vector3(width / 2, 0.1f, 0), new Vector3(0.5f));
			track.Position = new Vector3(0, -0.5f, -10f);
			modelsManager.AddVolume(track);
		}

		#endregion Constructors

		#region Properties

		/// <summary>
		/// Width of track.
		/// </summary>
		public float Width { get; private set; }

		/// <summary>
		/// How far ahead pillar rows spawn.
		/// </summary>
		public float SpawnPoint { get; private set; }

		/// <summary>
		/// Speed of pillars in units/second.
		/// </summary>
		public float Speed { get; private set; }

		/// <summary>
		/// Minium time between rows of pillars are spawned.
		/// </summary>
		public float MinRespawnTime { get; private set; }

		/// <summary>
		/// Maximum time between rows of pillars are spawned.
		/// </summary>
		public float MaxRespawnTime { get; private set; }

		/// <summary>
		/// Maximum number of pillars in row.
		/// </summary>
		public int MaxInRow { get; private set; }

		/// <summary>
		/// Minimum number of pillars in row.
		/// </summary>
		public int MinInRow { get; private set; }

		/// <summary>
		/// Queue used for holding all pillars and looping them around when new row is to be spawned.
		/// </summary>
		private Queue<Pillar> Pillars { get; set; }

		#endregion Properties

		#region Methods

		/// <summary>
		/// Updates track state.
		/// </summary>
		/// <param name="dt">Frame time.</param>
		/// <param name="player">Active player.</param>
		/// <param name="playerCollidedInThisUpdate">returns true if collision started to happen on this updatate.</param>
		public void Update(TimeSpan dt, Player player, out bool playerCollidedInThisUpdate)
		{
			tillNextRowSpanSeconds -= dt.TotalSeconds;
			if (tillNextRowSpanSeconds <= 0)
			{
				int inThisRow = random.Next(MinInRow, MaxInRow + 1);

				for (int i = 0; i < inThisRow; i++)
				{
					var pillar = Pillars.Dequeue();
					float x = (float)random.NextDouble(-Width / 2, Width / 2);
					pillar.Position = new Vector3(x, -0.5f, SpawnPoint);
					Pillars.Enqueue(pillar);
				}

				tillNextRowSpanSeconds = (float)random.NextDouble(MinRespawnTime, MaxRespawnTime);
			}

			foreach (var pillar in Pillars)
			{
				pillar.Position.Z -= (float)(dt.TotalSeconds * Speed);
				pillar.Update(dt);
			}

			//Collision
			bool isCollsion = IsPlayerCollidingWithPillar(player);
			if (wasCollision && isCollsion)
			{
				playerCollidedInThisUpdate = false;
			}
			else if (wasCollision && !isCollsion)
			{
				playerCollidedInThisUpdate = false;
			}
			else if (!wasCollision && isCollsion)
			{
				playerCollidedInThisUpdate = true;
			}
			else
			{
				playerCollidedInThisUpdate = false;
			}
			wasCollision = isCollsion;
		}

		/// <summary>
		/// Returns true is player is colliding with any fillar.
		/// </summary>
		/// <param name="player">Active player.</param>
		/// <returns>True if player is colliding with any fillar.</returns>
		public bool IsPlayerCollidingWithPillar(Player player)
		{
			foreach (var pillar in Pillars)
			{
				float dist = (pillar.Position - player.Position).Length;
				if (dist <= Math.Max(pillar.Collision2dFromPosition.X, pillar.Collision2dFromPosition.Y) +
					Math.Max(player.Collision2dFromPosition.X, player.Collision2dFromPosition.Y))
				{
					float pillarLeft = pillar.Position.X + pillar.Collision2dFromPosition.X / 2.0f;
					float pillarRight = pillar.Position.X - pillar.Collision2dFromPosition.X / 2.0f;
					float playerLeft = player.Position.X + player.Collision2dFromPosition.X / 2.0f;
					float playerRight = player.Position.X - player.Collision2dFromPosition.X / 2.0f;

					if (playerRight < pillarRight && playerRight > pillarLeft)
						return true;

					if (playerLeft > pillarLeft && playerLeft < pillarRight)
						return true;

					if (playerLeft < pillarLeft && playerRight > pillarRight)
						return true;

					float pillarBottom = pillar.Position.Z - pillar.Collision2dFromPosition.Y / 2.0f;
					float pillarTop = pillar.Position.Z + pillar.Collision2dFromPosition.Y / 2.0f;
					float playerBottom = player.Position.Z - player.Collision2dFromPosition.Y / 2.0f;
					float playerTop = player.Position.Z + player.Collision2dFromPosition.Y / 2.0f;

					if (playerTop < pillarTop && playerTop > pillarBottom)
						return true;

					if (playerBottom > pillarBottom && playerBottom < pillarTop)
						return true;

					if (playerBottom < pillarBottom && playerTop > pillarTop)
						return true;
				}
			}

			return false;
		}

		#endregion Methods
	}
}
