﻿using System;

namespace PowerPlane.Core
{
	/// <summary>
	/// Class holding all used extensions for this project.
	/// </summary>
	internal static class Extensions
	{
		#region Methods

		/// <summary>
		/// Returns next random double from min to max.
		/// </summary>
		/// <param name="that">Random object.</param>
		/// <param name="min">Minimum value returned.</param>
		/// <param name="max">Maximum value returned.</param>
		/// <returns>Random value between min and max.</returns>
		public static double NextDouble(this Random that, double min, double max)
		{
			double r = that.NextDouble();
			r *= max - min;
			r += min;

			return r;
		}

		#endregion Methods
	}
}
