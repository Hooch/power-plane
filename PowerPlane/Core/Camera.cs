﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;

namespace PowerPlane.Core
{
	public class Camera
	{
		#region Fields

		/// <summary>
		/// Positoin of the camra.
		/// </summary>
		public Vector3 Position = Vector3.Zero;

		/// <summary>
		/// Target the camera looks at in world coordinate system.
		/// </summary>
		public Vector3 LookAtTarget = new Vector3(0, 0, 1f);

		/// <summary>
		/// Perspecitive matrix.
		/// </summary>
		public Matrix4 PerspectiveFoVMatrix;

		private OpenTK.GameWindow gameWindow;

		#endregion Fields

		#region Constructors

		/// <summary>
		/// Creates new instance of camera and generates FoV matrix.
		/// </summary>
		/// <param name="gameWnd">Active game window.</param>
		public Camera(OpenTK.GameWindow gameWnd)
		{
			gameWindow = gameWnd;
			gameWindow.Resize += GameWindow_Resize;
			Position = Vector3.Zero;
			RegeneratePerspectiveFoVMatrix();
		}

		#endregion Constructors

		#region Methods

		/// <summary>
		/// Create a view matrix for this Camera
		/// </summary>
		/// <returns>A view matrix to look in the camera's direction</returns>
		public Matrix4 GetViewMatrix()
		{
			return Matrix4.LookAt(Position, LookAtTarget, Vector3.UnitY);
		}

		/// <summary>
		/// Handles resize event to regenerate perspective Matrix.
		/// </summary>
		private void GameWindow_Resize(object sender, EventArgs e)
		{
			RegeneratePerspectiveFoVMatrix();
		}

		/// <summary>
		/// Regenerates Perspecitve FoV Matrix.
		/// </summary>
		private void RegeneratePerspectiveFoVMatrix()
		{
			GL.Viewport(gameWindow.ClientRectangle);
			PerspectiveFoVMatrix = Matrix4.CreatePerspectiveFieldOfView(1.3f, gameWindow.Width / (float)gameWindow.Height, 0.1f, 128.0f);
		}

		#endregion Methods
	}
}
