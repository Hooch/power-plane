﻿using System;
using System.Collections.Generic;
using OpenTK.Input;
using PowerPlane.Managers;

namespace PowerPlane.Level
{
	partial class FirstLevel
	{
		#region Classes

		/// <summary>
		/// Handles all user input for first level.
		/// </summary>
		private class FirstLevelInput
		{
			#region Fields

			/// <summary>
			/// Holds registred commands for this level.
			/// </summary>
			private readonly KeyboardInputCommandManager keyboardManager;

			#endregion Fields

			#region Constructors

			/// <summary>
			/// Creates and registers commands for this level.
			/// </summary>
			public FirstLevelInput()
			{
				CommandExit = new KeyboardInputCommand("exit", Key.Escape);
				CommandStrafeLeft = new KeyboardInputCommand("strafe_left", Key.Left);
				CommandStrafeRight = new KeyboardInputCommand("strafe_right", Key.Right);

				var commandList = new List<KeyboardInputCommand>();
				commandList.Add(CommandExit);
				commandList.Add(CommandStrafeLeft);
				commandList.Add(CommandStrafeRight);

				keyboardManager = new KeyboardInputCommandManager(commandList);
			}

			#endregion Constructors

			#region Properties

			/// <summary>
			/// Describes state of Exit command.
			/// Exit command informs when game is requested to shutdown.
			/// </summary>
			public KeyboardInputCommand CommandExit { get; private set; }

			/// <summary>
			/// Describes state of strafe left command.
			/// Strafe left command request player movemnt to the left.
			/// </summary>
			public KeyboardInputCommand CommandStrafeLeft { get; private set; }

			/// <summary>
			/// Describes state of strafe right command.
			/// Strafe right command request player movemnt to the right.
			/// </summary>
			public KeyboardInputCommand CommandStrafeRight { get; private set; }

			#endregion Properties

			#region Methods

			/// <summary>
			/// Updates commands state in simulation step.
			/// </summary>
			/// <param name="dt">Simulation step time.</param>
			public void Update(TimeSpan dt)
			{
				var keyboardState = Keyboard.GetState();
				keyboardManager.UpdateInput(keyboardState);
			}

			#endregion Methods
		}

		#endregion Classes
	}
}
