﻿using System;

namespace PowerPlane.Level
{
	public interface ILevel
	{
		#region Methods

		/// <summary>
		/// Loads level and all level related assets.
		/// </summary>
		/// <param name="gameWindow"></param>
		void Load(OpenTK.GameWindow gameWindow);

		/// <summary>
		/// Unloads level and all level related assets.
		/// </summary>
		void Unload();

		/// <summary>
		/// Updates level state.
		/// </summary>
		/// <param name="dt">Frame time.</param>
		void Update(TimeSpan dt);

		/// <summary>
		/// Renders all level geometry.
		/// </summary>
		/// <param name="gameWindow"></param>
		void Render(OpenTK.GameWindow gameWindow);

		#endregion Methods
	}
}
