﻿using System;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using PowerPlane.Core;
using PowerPlane.Managers;
using PowerPlane.Entities;
using System.Media;

namespace PowerPlane.Level
{
	/// <summary>
	/// Describes and handles first level logic.
	/// </summary>
	partial class FirstLevel : ILevel
	{
		#region Fields

		/// <summary>
		/// Manager for all renderable models in this level.
		/// </summary>
		private ModelsManager levelModels;

		/// <summary>
		/// Used for handling input commands for this level.
		/// </summary>
		private FirstLevelInput input;

		/// <summary>
		/// Player entity.
		/// </summary>
		private Player player;

		/// <summary>
		/// Level camera.
		/// </summary>
		private Camera camera;

		/// <summary>
		/// Binding describing movemnt of camera relative to player.
		/// </summary>
		private CameraPlayerBinding cameraBinding;

		/// <summary>
		/// Main pillar race track.
		/// Handles player collsions with moving pillars.
		/// </summary>
		private PillarRaceTrack raceTrack;

		/// <summary>
		/// Player preloaded with hit sound.
		/// </summary>
		private SoundPlayer hitSoundPlayer;

		#endregion Fields

		#region Methods

		/// <summary>
		/// Loads level and all level related assets.
		/// </summary>
		/// <param name="gameWindow"></param>
		public void Load(OpenTK.GameWindow gameWindow)
		{
			levelModels = new ModelsManager();
			input = new FirstLevelInput();
			camera = new Camera(gameWindow);
			player = new Player(15f, new Vector2(10f));
			player.Collision2dFromPosition = new Vector2(1f, 1f);
			levelModels.AddEntity(player);

			hitSoundPlayer = new SoundPlayer(@"Assets\Sounds\hit2.wav");
			hitSoundPlayer.Load();

			cameraBinding = new CameraPlayerBinding(camera, player);

			raceTrack = new PillarRaceTrack(30f, 100, 40, 0.25f, 0.55f, 4, 6, levelModels);

			levelModels.LockModels();

			GL.Enable(EnableCap.DepthTest);
			GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
		}

		/// <summary>
		/// Unloads level and all assets. As this is only one level in this game this function was ommited.
		/// </summary>
		public void Unload()
		{
		}

		/// <summary>
		/// Update level state in simultion step.
		/// </summary>
		/// <param name="dt">Simulation step time.</param>
		public void Update(TimeSpan dt)
		{
			input.Update(dt);
			if (input.CommandExit.IsOn)
				Environment.Exit(0);

			if (!(input.CommandStrafeLeft.IsOn && input.CommandStrafeRight.IsOn))
			{
				if (input.CommandStrafeLeft.IsOn)
					player.MoveLeft(dt);
				else if (input.CommandStrafeRight.IsOn)
					player.MoveRight(dt);
			}

			player.Update(dt);
			bool isCollision;
			raceTrack.Update(dt, player, out isCollision);

			if (isCollision)
				hitSoundPlayer.Play();

			cameraBinding.UpdateCamera(dt);

			levelModels.Update(dt, camera);
		}

		/// <summary>
		/// Draws all level assets.
		/// </summary>
		/// <param name="gameWindow">Active game window.</param>
		public void Render(OpenTK.GameWindow gameWindow)
		{
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
			GL.ClearColor(Color.DarkBlue);

			levelModels.RenderModels();
		}

		#endregion Methods
	}
}
