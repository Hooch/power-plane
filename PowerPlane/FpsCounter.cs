﻿using System;

namespace PowerPlane
{
	/// <summary>
	/// Used for calculating fps.
	/// </summary>
	public class FpsCounter
	{
		#region Fields

		/// <summary>
		/// Factor of new frame time for all frames time.
		/// </summary>
		private double newFrameTimeFactor;

		/// <summary>
		/// Factor for old frame time.
		/// </summary>
		private double oldFrameTimeFactor;

		/// <summary>
		/// Accumulatr that hold time in milliseconds.
		/// </summary>
		private double accumulatorInMills;

		#endregion Fields

		#region Constructors

		/// <summary>
		/// Creates new istance of fps counter.
		/// </summary>
		/// <param name="framesForAverage"></param>
		public FpsCounter(int framesForAverage)
		{
			SetAverageFrames(framesForAverage);
		}

		#endregion Constructors

		#region Methods

		/// <summary>
		/// Set on how many frams average fps calculations.
		/// </summary>
		/// <param name="frames">Number of frames to average fps calculations on.</param>
		public void SetAverageFrames(int frames)
		{
			newFrameTimeFactor = 1.0 / frames;
			oldFrameTimeFactor = 1.0 - newFrameTimeFactor;
		}

		/// <summary>
		/// Gets average frame time.
		/// </summary>
		/// <returns>Average frame time in last <see cref="SetAverageFrames(int)"/>few frames.</returns>
		public TimeSpan GetAverageFrameTime()
		{
			return TimeSpan.FromMilliseconds(accumulatorInMills);
		}

		/// <summary>
		/// Gets average fps in last few frames.
		/// </summary>
		/// <returns>Inverted frame time. Frames per second.</returns>
		public double GetAverageFps()
		{
			return 1.0 / GetAverageFrameTime().Seconds;
		}

		/// <summary>
		/// Updates state of counter in render step.
		/// </summary>
		/// <param name="frameTime">Render frame time.</param>
		/// <returns>Average frame time for last frames.</returns>
		public TimeSpan Update(TimeSpan frameTime)
		{
			accumulatorInMills = accumulatorInMills * oldFrameTimeFactor + frameTime.Milliseconds * newFrameTimeFactor;
			return TimeSpan.FromMilliseconds(accumulatorInMills);
		}

		#endregion Methods
	}
}
