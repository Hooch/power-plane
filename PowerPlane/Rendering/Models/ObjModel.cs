﻿using System;
using OpenTK;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Globalization;

namespace PowerPlane.Rendering.Models
{
	/// <summary>
	/// Represents model loaded from OBJ file.
	/// </summary>
	internal class ObjModel : Volume
	{
		#region Fields

		/// <summary>
		/// Color data.
		/// </summary>
		private Vector3[] colorData;

		/// <summary>
		/// Vertex data.
		/// </summary>
		private Vector3[] verts;

		/// <summary>
		/// Indices data.
		/// </summary>
		private int[] indices;

		#endregion Fields

		#region Constructors

		/// <summary>
		/// Creates new vlolume form obj file.
		/// </summary>
		/// <param name="objFileStream">Strem to obj file.</param>
		/// <param name="color">Color of object.</param>
		/// <param name="relativeCenter">Relative center.</param>
		public ObjModel(Stream objFileStream, Vector3 color, Vector3 relativeCenter)
		{
			LoadDataFromFile(objFileStream);

			colorData = new Vector3[ColorDataCount];
			for (int i = 0; i < ColorDataCount; i++)
			{
				colorData[i] = new Vector3(color);
			}

			for (int i = 0; i < verts.Length; i++)
			{
				verts[i] -= relativeCenter;
			}
		}

		#endregion Constructors

		#region Methods

		/// <summary>
		/// Calclates model matrix based on rotation, scale and translation.
		/// </summary>
		public override void CalculateModelMatrix()
		{
			ModelMatrix = Matrix4.CreateScale(Scale) * Matrix4.CreateRotationX(Rotation.X) * Matrix4.CreateRotationY(Rotation.Y) * Matrix4.CreateRotationZ(Rotation.Z) * Matrix4.CreateTranslation(Position);
		}

		/// <summary>
		/// Gets color data.
		/// </summary>
		/// <returns>Color data.</returns>
		public override Vector3[] GetColorData()
		{
			return colorData;
		}

		/// <summary>
		/// Gets indices for this model.
		/// </summary>
		/// <param name="offset">Adds offset to indices.</param>
		/// <returns>Indices for this model with added offset.</returns>
		public override int[] GetIndices(int offset = 0)
		{
			int[] tempIndices = new int[indices.Length];
			indices.CopyTo(tempIndices, 0);

			if (offset != 0)
			{
				for (int i = 0; i < tempIndices.Length; i++)
				{
					tempIndices[i] += offset;
				}
			}

			return tempIndices;
		}

		/// <summary>
		/// Gets vertex data.
		/// </summary>
		/// <returns>Vertex data.</returns>
		public override Vector3[] GetVerts()
		{
			return verts;
		}

		/// <summary>
		/// Loads all model data from obj file.
		/// </summary>
		/// <param name="objFileStream">Obj file strem. Fuction does not close stream.</param>
		private void LoadDataFromFile(Stream objFileStream)
		{
			Regex vertRegex = new Regex(@"v\s+?(?<x>[\-\d\.]+)\s(?<y>[\-\d\.]+)\s(?<z>[\-\d\.]+)", RegexOptions.Compiled | RegexOptions.CultureInvariant);
			Regex faceRegex = new Regex(@"f\s+?(?<a>[\d]+)\s(?<b>[\d]+)\s(?<c>[\d]+)", RegexOptions.Compiled | RegexOptions.CultureInvariant);

			List<Vector3> vertList = new List<Vector3>();
			List<int> indicesList = new List<int>();

			StreamReader reader = new StreamReader(objFileStream);
			string line;
			while ((line = reader.ReadLine()) != null)
			{
				if (line.StartsWith("v"))
				{
					Match match = vertRegex.Match(line);
					float x = float.Parse(match.Groups["x"].ToString(), CultureInfo.InvariantCulture);
					float y = float.Parse(match.Groups["y"].ToString(), CultureInfo.InvariantCulture);
					float z = float.Parse(match.Groups["z"].ToString(), CultureInfo.InvariantCulture);

					vertList.Add(new Vector3(z, x, y));
				}
				else if (line.StartsWith("f"))
				{
					Match match = faceRegex.Match(line);
					int a = int.Parse(match.Groups["a"].ToString());
					int b = int.Parse(match.Groups["c"].ToString());
					int c = int.Parse(match.Groups["b"].ToString());

					indicesList.AddRange(new int[] { a - 1, b - 1, c - 1 });
				}
			}

			verts = vertList.ToArray();
			indices = indicesList.ToArray();

			VertCount = verts.Length;
			IndiceCount = indices.Length;
			ColorDataCount = verts.Length;
		}

		#endregion Methods
	}
}
