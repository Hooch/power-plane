﻿using System;
using OpenTK;

namespace PowerPlane.Rendering.Models
{
	/// <summary>
	/// Represents cuboid model.
	/// </summary>
	internal class Cuboid : Volume
	{
		#region Fields

		/// <summary>
		/// Vertex color data.
		/// </summary>
		private Vector3[] colorData;

		/// <summary>
		/// Vertex data.
		/// </summary>
		private Vector3[] verts;

		/// <summary>
		/// Indices data.
		/// </summary>
		private int[] indices;

		#endregion Fields

		#region Constructors

		/// <summary>
		/// Creates new cuboid of size 1,1,1.
		/// </summary>
		public Cuboid()
		{
			Initialize();

			colorData = new Vector3[8];
			for (int i = 0; i < 8; i++)
			{
				colorData[i] = new Vector3(1f);
			}
		}

		/// <summary>
		/// Creates new cuboid of given color and size 1,1,1.
		/// </summary>
		/// <param name="color"></param>
		public Cuboid(Vector3 color)
		{
			Initialize();

			colorData = new Vector3[8];
			for (int i = 0; i < 8; i++)
			{
				colorData[i] = new Vector3(color);
			}
		}

		/// <summary>
		/// Creates new cuboid with given color, size and center point.
		/// </summary>
		/// <param name="size">Cuboid size.</param>
		/// <param name="relativeCenter">Relative center.</param>
		/// <param name="color">Cuboid color.</param>
		public Cuboid(Vector3 size, Vector3 relativeCenter, Vector3 color)
		{
			Initialize();

			Dimension = size;
			RelativeCenter = relativeCenter;

			colorData = new Vector3[8];
			for (int i = 0; i < 8; i++)
			{
				colorData[i] = new Vector3(color);
			}

			for (int i = 0; i < verts.Length; i++)
			{
				verts[i] *= Dimension;
				verts[i] -= RelativeCenter;
			}
		}

		#endregion Constructors

		#region Properties

		/// <summary>
		/// Size of cuboid.
		/// </summary>
		public Vector3 Dimension { get; private set; }

		/// <summary>
		/// Relative center.
		/// </summary>
		public Vector3 RelativeCenter { get; private set; }

		#endregion Properties

		#region Methods

		/// <summary>
		/// Gets vertex data.
		/// </summary>
		/// <returns></returns>
		public override Vector3[] GetVerts()
		{
			return verts;
		}

		/// <summary>
		/// Get indices data.
		/// </summary>
		/// <param name="offset">Indices offset.</param>
		/// <returns>Indices for given volume with added offset.</returns>
		public override int[] GetIndices(int offset = 0)
		{
			int[] tempIndices = new int[indices.Length];
			indices.CopyTo(tempIndices, 0);

			if (offset != 0)
			{
				for (int i = 0; i < tempIndices.Length; i++)
				{
					tempIndices[i] += offset;
				}
			}

			return tempIndices;
		}

		/// <summary>
		/// Gets color data.
		/// </summary>
		/// <returns>Color data.</returns>
		public override Vector3[] GetColorData()
		{
			return colorData;
		}

		/// <summary>
		/// Calculates model matrix based on size, rotation and translation.
		/// </summary>
		public override void CalculateModelMatrix()
		{
			ModelMatrix = Matrix4.CreateScale(Scale) * Matrix4.CreateRotationX(Rotation.X) * Matrix4.CreateRotationY(Rotation.Y) * Matrix4.CreateRotationZ(Rotation.Z) * Matrix4.CreateTranslation(Position);
		}

		/// <summary>
		/// Initializez all data for volume.
		/// </summary>
		private void Initialize()
		{
			VertCount = 8;
			IndiceCount = 36;
			ColorDataCount = 8;

			verts = new Vector3[8] {
				new Vector3(0f, 0f, 0f),
				new Vector3(1f, 0f,  0f),
				new Vector3(1f, 1f,  0f),
				new Vector3(0f, 1f,  0f),
				new Vector3(0f, 0f,  1f),
				new Vector3(1f, 0f,  1f),
				new Vector3(1f, 1f,  1f),
				new Vector3(0f, 1f,  1f),
			};

			indices = new int[]
			{
				//left
				0, 2, 1,
				0, 3, 2,
				//back
				1, 2, 6,
				6, 5, 1,
				//right
				4, 5, 6,
				6, 7, 4,
				//top
				2, 3, 6,
				6, 3, 7,
				//front
				0, 7, 3,
				0, 4, 7,
				//bottom
				0, 1, 5,
				0, 5, 4
			};
		}

		#endregion Methods
	}
}
