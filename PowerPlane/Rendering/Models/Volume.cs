﻿using OpenTK;

namespace PowerPlane.Rendering.Models
{
	/// <summary>
	/// An object made up of vertices
	/// </summary>
	public abstract class Volume
	{
		#region Fields

		/// <summary>
		/// Volume positon.
		/// </summary>
		public Vector3 Position = Vector3.Zero;

		/// <summary>
		/// Volume rotation around volumes 0,0,0 in radians.
		/// </summary>
		public Vector3 Rotation = Vector3.Zero;

		/// <summary>
		/// Scale. 1 is 100%
		/// </summary>
		public Vector3 Scale = Vector3.One;

		/// <summary>
		/// Vertex count.
		/// </summary>
		public int VertCount;

		/// <summary>
		/// Indices count.
		/// </summary>
		public int IndiceCount;

		/// <summary>
		/// Color data count. Should be the same as Vertex count.
		/// </summary>
		public int ColorDataCount;

		/// <summary>
		/// Model matrix.
		/// </summary>
		public Matrix4 ModelMatrix = Matrix4.Identity;

		/// <summary>
		/// View projecion matrix.
		/// </summary>
		public Matrix4 ViewProjectionMatrix = Matrix4.Identity;

		/// <summary>
		/// Model view projection matrix.
		/// </summary>
		public Matrix4 ModelViewProjectionMatrix = Matrix4.Identity;

		#endregion Fields

		#region Methods

		/// <summary>
		/// Gets vertex data.
		/// </summary>
		/// <returns>Return vertex data.</returns>
		public abstract Vector3[] GetVerts();

		/// <summary>
		/// Gets indices data.
		/// </summary>
		/// <param name="offset">Offset for indices.</param>
		/// <returns>Return indices data with added offset.</returns>
		public abstract int[] GetIndices(int offset = 0);

		/// <summary>
		/// Gets color data.
		/// </summary>
		/// <returns>Color data.</returns>
		public abstract Vector3[] GetColorData();

		/// <summary>
		/// Calculates model matrix using Rotation, scale and translation.
		/// </summary>
		public abstract void CalculateModelMatrix();

		#endregion Methods
	}
}
