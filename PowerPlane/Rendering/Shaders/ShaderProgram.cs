﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK.Graphics.OpenGL;
using System.IO;

namespace PowerPlane.Rendering.Shaders
{
	internal class ShaderProgram
	{
		#region Fields

		/// <summary>
		/// Shader attributes
		/// </summary>
		public Dictionary<String, AttributeInfo> Attributes = new Dictionary<string, AttributeInfo>();

		/// <summary>
		/// Shader uniform values.
		/// </summary>
		public Dictionary<String, UniformInfo> Uniforms = new Dictionary<string, UniformInfo>();

		/// <summary>
		/// Buffers.
		/// </summary>
		public Dictionary<String, uint> Buffers = new Dictionary<string, uint>();

		#endregion Fields

		#region Constructors

		/// <summary>
		/// Creates new instace of shader program.
		/// </summary>
		public ShaderProgram()
		{
			ProgramID = GL.CreateProgram();

			ProgramID = -1;
			VShaderID = -1;
			FShaderID = -1;
			AttributeCount = 0;
			UniformCount = 0;
		}

		/// <summary>
		/// Creates new instance of shader program with specified values.
		/// </summary>
		/// <param name="vshader">Vertex shader file path or file contents.</param>
		/// <param name="fshader">Fragment shader file path or file contents.</param>
		/// <param name="fromFile">Are shader parametrs paths or file contents.</param>
		public ShaderProgram(String vshader, String fshader, bool fromFile = false)
		{
			ProgramID = GL.CreateProgram();

			if (fromFile)
			{
				LoadShaderFromFile(vshader, ShaderType.VertexShader);
				LoadShaderFromFile(fshader, ShaderType.FragmentShader);
			}
			else
			{
				LoadShaderFromString(vshader, ShaderType.VertexShader);
				LoadShaderFromString(fshader, ShaderType.FragmentShader);
			}

			Link();
			GenBuffers();
		}

		#endregion Constructors

		#region Properties

		/// <summary>
		/// Program id as registred in OpenGL.
		/// </summary>
		public int ProgramID { get; protected set; }

		/// <summary>
		/// Vertex shader id.
		/// </summary>
		public int VShaderID { get; protected set; }

		/// <summary>
		/// Fragment shader id.
		/// </summary>
		public int FShaderID { get; protected set; }

		/// <summary>
		/// Number of attributes.
		/// </summary>
		public int AttributeCount { get; protected set; }

		/// <summary>
		/// Number of uniform values.
		/// </summary>
		public int UniformCount { get; protected set; }

		#endregion Properties

		#region Methods

		/// <summary>
		/// Loads shader from code.
		/// </summary>
		/// <param name="code">Shader code.</param>
		/// <param name="type">Type of shader.</param>
		public void LoadShaderFromString(String code, ShaderType type)
		{
			if (type == ShaderType.VertexShader)
			{
				int shaderId;
				loadShader(code, type, out shaderId);
				VShaderID = shaderId;
			}
			else if (type == ShaderType.FragmentShader)
			{
				int shaderId;
				loadShader(code, type, out shaderId);
				FShaderID = shaderId;
			}
		}

		/// <summary>
		/// Loads shader from file.
		/// </summary>
		/// <param name="filename">Shader file path.</param>
		/// <param name="type">Type of shader.</param>
		public void LoadShaderFromFile(String filename, ShaderType type)
		{
			using (StreamReader sr = new StreamReader(filename))
			{
				if (type == ShaderType.VertexShader)
				{
					int shaderId;
					loadShader(sr.ReadToEnd(), type, out shaderId);
					VShaderID = shaderId;
				}
				else if (type == ShaderType.FragmentShader)
				{
					int shaderId;
					loadShader(sr.ReadToEnd(), type, out shaderId);
					VShaderID = FShaderID;
				}
			}
		}

		/// <summary>
		/// Links loaded shaders into shader program.
		/// </summary>
		public void Link()
		{
			GL.LinkProgram(ProgramID);

			Console.WriteLine(GL.GetProgramInfoLog(ProgramID));

			int tempOut;
			GL.GetProgram(ProgramID, GetProgramParameterName.ActiveAttributes, out tempOut);
			AttributeCount = tempOut;

			GL.GetProgram(ProgramID, GetProgramParameterName.ActiveUniforms, out tempOut);
			UniformCount = tempOut;

			for (int i = 0; i < AttributeCount; i++)
			{
				AttributeInfo info = new AttributeInfo();
				int length = 0;

				StringBuilder name = new StringBuilder();

				GL.GetActiveAttrib(ProgramID, i, 256, out length, out info.Size, out info.Type, name);

				info.Name = name.ToString();
				info.Address = GL.GetAttribLocation(ProgramID, info.Name);
				Attributes.Add(name.ToString(), info);
			}

			for (int i = 0; i < UniformCount; i++)
			{
				UniformInfo info = new UniformInfo();
				int length = 0;

				StringBuilder name = new StringBuilder();

				GL.GetActiveUniform(ProgramID, i, 256, out length, out info.Size, out info.Type, name);

				info.Name = name.ToString();
				Uniforms.Add(name.ToString(), info);
				info.Address = GL.GetUniformLocation(ProgramID, info.Name);
			}
		}

		/// <summary>
		/// Generates buffers.
		/// </summary>
		public void GenBuffers()
		{
			for (int i = 0; i < Attributes.Count; i++)
			{
				uint buffer = 0;
				GL.GenBuffers(1, out buffer);

				Buffers.Add(Attributes.Values.ElementAt(i).Name, buffer);
			}

			for (int i = 0; i < Uniforms.Count; i++)
			{
				uint buffer = 0;
				GL.GenBuffers(1, out buffer);

				Buffers.Add(Uniforms.Values.ElementAt(i).Name, buffer);
			}
		}

		/// <summary>
		/// Enables vertex attribute arrays.
		/// </summary>
		public void EnableVertexAttribArrays()
		{
			for (int i = 0; i < Attributes.Count; i++)
			{
				GL.EnableVertexAttribArray(Attributes.Values.ElementAt(i).Address);
			}
		}

		/// <summary>
		/// Disables vertex attribute arrays.
		/// </summary>
		public void DisableVertexAttribArrays()
		{
			for (int i = 0; i < Attributes.Count; i++)
			{
				GL.DisableVertexAttribArray(Attributes.Values.ElementAt(i).Address);
			}
		}

		/// <summary>
		/// Retuns id of attribute of given name.
		/// </summary>
		/// <param name="name">Name of attribute.</param>
		/// <returns>Id of attribute.</returns>
		public int GetAttribute(string name)
		{
			if (Attributes.ContainsKey(name))
			{
				return Attributes[name].Address;
			}
			else
			{
				return -1;
			}
		}

		/// <summary>
		/// Gets id of uniform value with given name.
		/// </summary>
		/// <param name="name">Name of uniform value.</param>
		/// <returns>Id of uniform value.</returns>
		public int GetUniform(string name)
		{
			if (Uniforms.ContainsKey(name))
			{
				return Uniforms[name].Address;
			}
			else
			{
				return -1;
			}
		}

		/// <summary>
		/// Gets id of buffer with given name.
		/// </summary>
		/// <param name="name">Buffer name.</param>
		/// <returns>Id of buffer.</returns>
		public uint GetBuffer(string name)
		{
			if (Buffers.ContainsKey(name))
			{
				return Buffers[name];
			}
			else
			{
				return 0;
			}
		}

		/// <summary>
		/// Loads shader and retuns address.
		/// Prints debug information to console.
		/// </summary>
		/// <param name="code">Shader code.</param>
		/// <param name="type">Shadertype.</param>
		/// <param name="address">Returns shader address.</param>
		private void loadShader(String code, ShaderType type, out int address)
		{
			address = GL.CreateShader(type);
			GL.ShaderSource(address, code);
			GL.CompileShader(address);
			GL.AttachShader(ProgramID, address);
			Console.WriteLine(GL.GetShaderInfoLog(address));
		}

		#endregion Methods

		#region Classes

		/// <summary>
		/// Holds inofrmation about uniform value.
		/// </summary>
		public class UniformInfo
		{
			#region Fields

			/// <summary>
			/// Name.
			/// </summary>
			public string Name = "";

			/// <summary>
			/// Address.
			/// </summary>
			public int Address = -1;

			/// <summary>
			/// Size in bytes.
			/// </summary>
			public int Size = 0;

			/// <summary>
			/// Type of uniform value.
			/// </summary>
			public ActiveUniformType Type;

			#endregion Fields
		}

		/// <summary>
		/// Holds information about attribute.
		/// </summary>
		public class AttributeInfo
		{
			#region Fields

			/// <summary>
			/// Name.
			/// </summary>
			public string Name = "";

			/// <summary>
			/// Address.
			/// </summary>
			public int Address = -1;

			/// <summary>
			/// Size in bytes.
			/// </summary>
			public int Size = 0;

			/// <summary>
			/// Type of attribute.
			/// </summary>
			public ActiveAttribType Type;

			#endregion Fields
		}

		#endregion Classes
	}
}
