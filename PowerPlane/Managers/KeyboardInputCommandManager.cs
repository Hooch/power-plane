﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using OpenTK.Input;

namespace PowerPlane.Managers
{
	/// <summary>
	/// Describes State of input.
	/// </summary>
	public enum KeyboardInputCommandState
	{
		/// <summary>
		/// Input is not in active state.
		/// </summary>
		NotActive,

		/// <summary>
		/// Input is active.
		/// </summary>
		Active,

		/// <summary>
		/// Input is on rising edge. Will be <see cref="Active"/> next iteration.
		/// </summary>
		RisingEdge,

		/// <summary>
		/// Input is on falling edge. Will be <see cref="NotActive"/> next iteration.
		/// </summary>
		FallingEdge
	}

	public class KeyboardInputCommandManager
	{
		#region Fields

		/// <summary>
		/// Dictionary of all registred commands.
		/// </summary>
		private readonly Dictionary<string, KeyboardInputCommand> inputCommands;

		#endregion Fields

		#region Constructors

		/// <summary>
		/// Creates new keyboard input manager.
		/// </summary>
		public KeyboardInputCommandManager()
		{
			inputCommands = new Dictionary<string, KeyboardInputCommand>();
		}

		/// <summary>
		/// Creates new keyboard input manager with given set of commands.
		/// </summary>
		/// <param name="inputCommandsList"></param>
		public KeyboardInputCommandManager(List<KeyboardInputCommand> inputCommandsList)
		{
			inputCommands = new Dictionary<string, KeyboardInputCommand>();

			try
			{
				inputCommandsList.ForEach(command => inputCommands.Add(command.Name, command));
			}
			catch (ArgumentException)
			{
				Trace.TraceError("InputCommandManager can only have one command with given name.");

				throw;
			}
		}

		#endregion Constructors

		#region Indexers

		/// <summary>
		/// Returns keyboard command with given name.
		/// </summary>
		/// <param name="name">Name of keyboard command to return.</param>
		/// <returns>Keyboard command with given name.</returns>
		public KeyboardInputCommand this[string name]
		{
			get
			{
				return inputCommands[name];
			}
		}

		#endregion Indexers

		#region Methods

		/// <summary>
		/// Updates input and all commands state.
		/// </summary>
		/// <param name="keyboard">Snapshot of keyboard state.</param>
		public void UpdateInput(KeyboardState keyboard)
		{
			foreach (var pair in inputCommands)
			{
				pair.Value.CalculateNewState(keyboard[pair.Value.KeyCode]);
			}
		}

		#endregion Methods
	}

	/// <summary>
	/// Describes keyboard command and its states.
	/// </summary>
	public class KeyboardInputCommand
	{
		#region Constructors

		/// <summary>
		/// Creates new keyboard command.
		/// </summary>
		/// <param name="name">Name of command.</param>
		/// <param name="key">Keyboard key bind with this command.</param>
		/// <param name="enabled">Initial state of command.</param>
		public KeyboardInputCommand(string name, Key key, bool enabled = true)
		{
			Name = name;
			KeyCode = key;
			Enabled = enabled;
			State = KeyboardInputCommandState.NotActive;
		}

		#endregion Constructors

		#region Properties

		/// <summary>
		/// command name.
		/// </summary>
		public string Name { get; private set; }

		/// <summary>
		/// Command key code.
		/// </summary>
		public Key KeyCode { get; set; }

		/// <summary>
		/// Command state.
		/// </summary>
		public bool Enabled { get; set; }
		public KeyboardInputCommandState State { get; set; }

		/// <summary>
		/// True when State is <see cref="KeyboardInputCommandState.Active"/> or <see cref="KeyboardInputCommandState.RisingEdge"/>.
		/// False when State is <see cref="KeyboardInputCommandState.NotActive"/> or <see cref="KeyboardInputCommandState.FallingEdge"/>
		/// </summary>
		public bool IsOn
		{
			get
			{
				return State == KeyboardInputCommandState.Active ||
					   State == KeyboardInputCommandState.RisingEdge;
			}
		}

		/// <summary>
		/// True when State is <see cref="KeyboardInputCommandState.NotActive"/> or <see cref="KeyboardInputCommandState.FallingEdge"/>
		/// False when State is <see cref="KeyboardInputCommandState.Active"/> or <see cref="KeyboardInputCommandState.RisingEdge"/>.
		/// </summary>
		public bool IsOff
		{
			get
			{
				return State == KeyboardInputCommandState.NotActive ||
					   State == KeyboardInputCommandState.FallingEdge;
			}
		}

		/// <summary>
		/// Returns true when state is <see cref="KeyboardInputCommandState.Active"/>. Except for <see cref="KeyboardInputCommandState.RisingEdge"/> and <see cref="KeyboardInputCommandState.FallingEdge"/>. Those return false.
		/// </summary>
		public bool IsActive
		{
			get { return State == KeyboardInputCommandState.Active; }
		}

		/// <summary>
		/// Returns true when state is <see cref="KeyboardInputCommandState.NotActive"/>. Except for <see cref="KeyboardInputCommandState.RisingEdge"/> and <see cref="KeyboardInputCommandState.FallingEdge"/>. Those return false.
		/// </summary>
		public bool IsNotActive
		{
			get { return State == KeyboardInputCommandState.NotActive; }
		}

		/// <summary>
		/// Returns true for State equal <see cref="KeyboardInputCommandState.RisingEdge"/>.
		/// Means that in previous iteration state was <see cref="KeyboardInputCommandState.NotActive"/>.
		/// Next iteration is guaranteed to be <see cref="KeyboardInputCommandState.Active"/>
		/// </summary>
		public bool IsRising
		{
			get { return State == KeyboardInputCommandState.RisingEdge; }
		}

		/// <summary>
		/// Returns true for State equal <see cref="KeyboardInputCommandState.FallingEdge"/>.
		/// Means that in previous iteration state was <see cref="KeyboardInputCommandState.Active"/>.
		/// Next iteration is guaranteed to be <see cref="KeyboardInputCommandState.NotActive"/>
		/// </summary>
		public bool IsFalling
		{
			get { return State == KeyboardInputCommandState.FallingEdge; }
		}

		#endregion Properties

		#region Methods

		/// <summary>
		/// Between two "edges" there is always stable (Active/NotActive) state.
		/// Even if it has to be inseretd there virtually.
		/// It is not noticable by user but makes logic much more simpler to program.
		/// </summary>
		/// <param name="keyState"></param>
		public void CalculateNewState(bool keyState)
		{
			switch (State)
			{
				case KeyboardInputCommandState.Active:
					if (keyState == false)
						State = KeyboardInputCommandState.FallingEdge;
					break;

				case KeyboardInputCommandState.NotActive:
					if (keyState == true)
						State = KeyboardInputCommandState.RisingEdge;
					break;

				case KeyboardInputCommandState.RisingEdge:
					State = KeyboardInputCommandState.Active;
					break;

				case KeyboardInputCommandState.FallingEdge:
					State = KeyboardInputCommandState.NotActive;
					break;
			}
		}

		#endregion Methods
	}
}
