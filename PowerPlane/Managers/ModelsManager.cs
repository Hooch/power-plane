﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using PowerPlane.Rendering.Shaders;
using PowerPlane.Rendering.Models;
using PowerPlane.Core;

namespace PowerPlane.Managers
{
	public class ModelsManager
	{
		#region Fields

		/// <summary>
		/// Vertex data.
		/// </summary>
		private Vector3[] vertdata;

		/// <summary>
		/// Coler vertex data.
		/// </summary>
		private Vector3[] colorData;

		/// <summary>
		/// Indices data.
		/// </summary>
		private int[] indiceData;

		/// <summary>
		/// Buffer data.
		/// </summary>
		private int ibo_elements;

		/// <summary>
		/// Currently active shader program.
		/// </summary>
		private string activeShader;

		/// <summary>
		/// Indicates wheater simulation step was run at least once.
		/// </summary>
		private bool wasUpdated = false;

		/// <summary>
		/// Indicates wheater all models are loaded into buffer and are ready for rendering.
		/// </summary>
		private bool modelsLoked = false;

		/// <summary>
		/// Holds inforamtion of all loaded shader programs.
		/// </summary>
		private Dictionary<string, ShaderProgram> shaders = new Dictionary<string, ShaderProgram>();

		/// <summary>
		/// Collection of loaded volumes.
		/// </summary>
		private List<Volume> Volumes = new List<Volume>();

		/// <summary>
		/// List of volumes rendered with default shader.
		/// </summary>
		private List<Volume> ObjectsDefault = new List<Volume>();

		#endregion Fields

		#region Constructors

		/// <summary>
		/// Creates model manager.
		/// </summary>
		public ModelsManager()
		{
			GL.GenBuffers(1, out ibo_elements);

			shaders.Add("default", new ShaderProgram("Assets\\Shaders\\default_vertex_shader.glsl", "Assets\\Shaders\\default_fragment_shader.glsl", true));

			activeShader = "default";
		}

		#endregion Constructors

		#region Methods

		/// <summary>
		/// Renders all modles in this manager.
		/// </summary>
		public void RenderModels()
		{
			if (!wasUpdated || !modelsLoked)
				return;

			shaders[activeShader].EnableVertexAttribArrays();
			int indiceat = 0;

			foreach (Volume volume in Volumes)
			{
				GL.UniformMatrix4(shaders[activeShader].GetUniform("modelview"), false, ref volume.ModelViewProjectionMatrix);

				GL.DrawElements(BeginMode.Triangles, volume.IndiceCount, DrawElementsType.UnsignedInt, indiceat * sizeof(uint));
				indiceat += volume.IndiceCount;
			}

			shaders[activeShader].DisableVertexAttribArrays();
		}

		/// <summary>
		/// Locks models and prevents adding new ones.
		/// </summary>
		public void LockModels()
		{
			List<Vector3> verts = new List<Vector3>();
			List<int> inds = new List<int>();
			List<Vector3> colors = new List<Vector3>();
			List<Vector2> texcoords = new List<Vector2>();

			// Assemble vertex and indice data for all volumes
			int vertcount = 0;
			foreach (Volume v in Volumes)
			{
				verts.AddRange(v.GetVerts().ToList());
				inds.AddRange(v.GetIndices(vertcount).ToList());
				colors.AddRange(v.GetColorData().ToList());
				vertcount += v.VertCount;
			}

			vertdata = verts.ToArray();
			indiceData = inds.ToArray();
			colorData = colors.ToArray();

			GL.BindBuffer(BufferTarget.ArrayBuffer, shaders[activeShader].GetBuffer("vPosition"));

			GL.BufferData<Vector3>(BufferTarget.ArrayBuffer, (IntPtr)(vertdata.Length * Vector3.SizeInBytes), vertdata, BufferUsageHint.StaticDraw);
			GL.VertexAttribPointer(shaders[activeShader].GetAttribute("vPosition"), 3, VertexAttribPointerType.Float, false, 0, 0);

			if (shaders[activeShader].GetAttribute("vColor") != -1)
			{
				GL.BindBuffer(BufferTarget.ArrayBuffer, shaders[activeShader].GetBuffer("vColor"));
				GL.BufferData<Vector3>(BufferTarget.ArrayBuffer, (IntPtr)(colorData.Length * Vector3.SizeInBytes), colorData, BufferUsageHint.StaticDraw);
				GL.VertexAttribPointer(shaders[activeShader].GetAttribute("vColor"), 3, VertexAttribPointerType.Float, true, 0, 0);
			}

			GL.UseProgram(shaders[activeShader].ProgramID);

			GL.BindBuffer(BufferTarget.ArrayBuffer, 0);

			// Buffer index data
			GL.BindBuffer(BufferTarget.ElementArrayBuffer, ibo_elements);
			GL.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(indiceData.Length * sizeof(int)), indiceData, BufferUsageHint.StaticDraw);

			modelsLoked = true;
		}

		/// <summary>
		/// Updates models state.
		/// </summary>
		public void Update(TimeSpan dt, Camera camera)
		{
			wasUpdated = true;

			foreach (Volume volume in Volumes)
			{
				volume.CalculateModelMatrix();
				volume.ViewProjectionMatrix = camera.GetViewMatrix() * camera.PerspectiveFoVMatrix;
				volume.ModelViewProjectionMatrix = volume.ModelMatrix * volume.ViewProjectionMatrix;
			}
		}

		/// <summary>
		/// Adds new entity to model manger.
		/// </summary>
		/// <param name="entity">Entity to add</param>
		/// <param name="shader">Shader to use for this entity.</param>
		public void AddEntity(Entities.RenderableEntity entity, string shader = "default")
		{
			AddVolume(entity.Volume, shader);
		}

		/// <summary>
		/// Adds volume to model manger.
		/// </summary>
		/// <param name="volume">Volume to add.</param>
		/// <param name="shader">Shader to use for this entity.</param>
		public void AddVolume(Volume volume, string shader = "default")
		{
			switch (shader)
			{
				case "default":
					ObjectsDefault.Add(volume);
					break;

				default:
					throw new ArgumentException("No shader with that name exists: " + shader);
			}

			Volumes.Add(volume);
		}

		#endregion Methods
	}
}
