#version 430 core

in vec4 color;
out vec4 outputColor;

void main()
{
	const float LOG2 = 1.442695;
	float z = gl_FragCoord.z / gl_FragCoord.w;

	if(color.r > 0.99 && color.g > 0.99 && color.b > 0.99)
	{
		float fogFactor = exp2( -0.7 * 0.7 * z * z * LOG2 );
		fogFactor = clamp(fogFactor, 0.0, 1.0);

		outputColor = mix(vec4(0.8, 0.4, 0.1, 0), vec4(0, 1, 1, 1), -fogFactor);
	}
	else
	{
		float fogFactor = exp2( -0.02 * 0.02 * z * z * LOG2 );
		fogFactor = clamp(fogFactor, 0.0, 1.0);

		outputColor = mix(vec4(0, 0, 0.55, 1), color, fogFactor );
	}
}