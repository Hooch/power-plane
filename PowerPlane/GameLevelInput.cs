﻿using System;
using System.Collections.Generic;
using OpenTK.Input;
using PowerPlane.Managers;

namespace PowerPlane
{
	partial class Game
	{
		#region Classes

		/// <summary>
		/// Describes input for top level game window.
		/// </summary>
		private class GameLevelInput
		{
			#region Fields

			/// <summary>
			/// Keyboard manager for main game window.
			/// </summary>
			private readonly KeyboardInputCommandManager keyboardManager;

			#endregion Fields

			#region Constructors

			/// <summary>
			/// Creates commands for main game window.
			/// </summary>
			public GameLevelInput()
			{
				CommandSwitchScreenMode = new KeyboardInputCommand("switch_screen_mode", Key.F11);

				var commandList = new List<KeyboardInputCommand>();
				commandList.Add(CommandSwitchScreenMode);

				keyboardManager = new KeyboardInputCommandManager(commandList);
			}

			#endregion Constructors

			#region Properties

			/// <summary>
			/// Describes command for swithing display mode between fullscreen and windwowd mode.
			/// </summary>
			public KeyboardInputCommand CommandSwitchScreenMode { get; private set; }

			#endregion Properties

			#region Methods

			/// <summary>
			/// Update commands state in simulation step.
			/// </summary>
			/// <param name="dt">Simulatio step time.</param>
			public void Update(TimeSpan dt)
			{
				var keyboardState = Keyboard.GetState();
				keyboardManager.UpdateInput(keyboardState);
			}

			#endregion Methods
		}

		#endregion Classes
	}
}
