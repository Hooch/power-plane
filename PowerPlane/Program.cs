﻿using System;
using System.Diagnostics;
using OpenTK;
using OpenTK.Graphics;

namespace PowerPlane
{
	internal class Program
	{
		#region Methods

		[STAThread]
		private static void Main(string[] args)
		{
			AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionTrapper;

			var game = new Game();
			var gameWindow = new OpenTK.GameWindow(1280, 720, new GraphicsMode(32, 24, 0, 4), "PowerPlane - Maciej Kuśnierz", GameWindowFlags.Default);

			gameWindow.Load += game.Load;
			gameWindow.UpdateFrame += game.Update;
			gameWindow.RenderFrame += game.Render;
			gameWindow.Run(200, 60);
		}

		private static void UnhandledExceptionTrapper(object sender, UnhandledExceptionEventArgs e)
		{
			Trace.TraceError(e.ExceptionObject.ToString());
			Console.WriteLine("\n\nPress [Enter] to continue...");
			Console.ReadLine();
			Environment.Exit(1);
		}

		#endregion Methods
	}
}
