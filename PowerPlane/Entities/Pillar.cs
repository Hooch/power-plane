﻿using System;
using PowerPlane.Rendering.Models;
using OpenTK;

namespace PowerPlane.Entities
{
	/// <summary>
	/// Represents pillar obstacle.
	/// </summary>
	internal class Pillar : RenderableEntity
	{
		#region Constructors

		/// <summary>
		/// Creates new instance of pillar.
		/// </summary>
		public Pillar()
		{
			Vector3 dimensions = new Vector3(0.5f, 50f, 0.5f);
			Vector3 relativeCenter = dimensions / 2f;
			relativeCenter.Y = 0;
			Collision2dFromPosition = new Vector2(dimensions.X, dimensions.Z);

			Volume = new Cuboid(dimensions, relativeCenter, new Vector3(0f, 0f, 0f));
		}

		#endregion Constructors

		#region Methods

		/// <summary>
		/// Update pillar in simulation step.
		/// </summary>
		/// <param name="dt">Simulation step time.</param>
		public override void Update(TimeSpan dt)
		{
			base.Update(dt);
			Volume.Position = Position;
		}

		#endregion Methods
	}
}
