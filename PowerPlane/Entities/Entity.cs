﻿using System;
using OpenTK;

namespace PowerPlane.Entities
{
	/// <summary>
	/// Base for all game entities.
	/// </summary>
	public abstract class Entity
	{
		#region Fields

		/// <summary>
		/// Entity position.
		/// </summary>
		public Vector3 Position;

		/// <summary>
		/// Entity rotation (in radians)
		/// </summary>
		public Vector3 Rotation;

		#endregion Fields

		#region Methods

		/// <summary>
		/// Update entity state in simulation step.
		/// </summary>
		/// <param name="dt">Simulation step time.</param>
		public abstract void Update(TimeSpan dt);

		#endregion Methods
	}
}
