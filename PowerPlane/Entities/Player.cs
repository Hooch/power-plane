﻿using OpenTK;
using PowerPlane.Rendering.Models;
using System;
using System.IO;

namespace PowerPlane.Entities
{
	/// <summary>
	/// Player entity.
	/// </summary>
	public class Player : RenderableEntity
	{
		#region Fields

		/// <summary>
		/// Current player velocity.
		/// </summary>
		private Vector2 Velocity = new Vector2();

		#endregion Fields

		#region Constructors

		/// <summary>
		/// Creates new instance of player object.
		/// </summary>
		/// <param name="maxSideDistance">Maximum allowed side movemnt distance.</param>
		/// <param name="friction">Friciton of player. Describes dumping force.</param>
		public Player(float maxSideDistance, Vector2 friction)
		{
			Volume = new ObjModel(File.OpenRead("Assets\\Models\\plane.obj"), new Vector3(1), Vector3.Zero);
			MaxSideDistance = maxSideDistance;
			Friction = friction;
		}

		#endregion Constructors

		#region Properties

		/// <summary>
		/// Player friction.
		/// </summary>
		public Vector2 Friction { get; set; }

		/// <summary>
		/// Maximum allowed side movemnt distance.
		/// </summary>
		public float MaxSideDistance { get; set; }

		#endregion Properties

		#region Methods

		/// <summary>
		/// Adds player velocity to move him left.
		/// </summary>
		/// <param name="dt">Simulation step time.</param>
		public void MoveLeft(TimeSpan dt)
		{
			if (Position.X >= MaxSideDistance - 3f)
				return;

			Velocity.X += 350 * (float)dt.TotalSeconds;
		}

		/// <summary>
		/// Adds player velocity to move him right.
		/// </summary>
		/// <param name="dt">Simulation step time.</param>
		public void MoveRight(TimeSpan dt)
		{
			if (Position.X <= -MaxSideDistance + 3f)
				return;

			Velocity.X -= 350 * (float)dt.TotalSeconds;
		}

		/// <summary>
		/// Updates player state in simulation step.
		/// </summary>
		/// <param name="dt">Simulation step time.</param>
		public override void Update(TimeSpan dt)
		{
			Velocity -= Velocity * Friction * (float)dt.TotalSeconds;

			Position.X += Velocity.X * (float)dt.TotalSeconds;

			if (Position.X > MaxSideDistance)
			{
				Position.X = MaxSideDistance;
				Velocity = -Velocity;
			}

			if (Position.X < -MaxSideDistance)
			{
				Position.X = -MaxSideDistance;
				Velocity = -Velocity;
			}

			Volume.Rotation.Z = (float)(Velocity.X * -0.01);

			base.Update(dt);
			Volume.Position = Position;
		}

		#endregion Methods
	}
}
