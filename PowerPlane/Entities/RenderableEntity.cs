﻿using System;
using PowerPlane.Rendering.Models;
using OpenTK;

namespace PowerPlane.Entities
{
	public class RenderableEntity : Entity
	{
		#region Properties

		/// <summary>
		/// 2d collsion model.
		/// </summary>
		public Vector2 Collision2dFromPosition { get; set; }

		/// <summary>
		/// Represtens renderable volume.
		/// </summary>
		public virtual Volume Volume { get; protected set; }

		#endregion Properties

		#region Methods

		/// <summary>
		/// Updates entity in simlation step.
		/// </summary>
		/// <param name="dt">Simulation step time.</param>
		public override void Update(TimeSpan dt)
		{
		}

		#endregion Methods
	}
}
