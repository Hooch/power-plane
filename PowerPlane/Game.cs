﻿using System;
using System.Diagnostics;
using OpenTK;
using PowerPlane.Level;

namespace PowerPlane
{
	public partial class Game
	{
		#region Fields

		private readonly Stopwatch frameStopwatch = new Stopwatch();
		private ILevel currentLevel;
		private GameLevelInput gameLevelInput = new GameLevelInput();

		#endregion Fields

		#region Methods

		/// <summary>
		/// Invoke once on Load from GameWindow class.
		/// </summary>
		public void Load(object sender, EventArgs eventArgs)
		{
			var gameWindow = sender as OpenTK.GameWindow;
			if (gameWindow == null)
				throw new ArgumentException("Sender needs to be of type GameWidnow.");

			gameWindow.VSync = VSyncMode.Adaptive;

			currentLevel = new FirstLevel();
			currentLevel.Load(gameWindow);
		}

		/// <summary>
		/// Invoke every time game logic needs update
		/// </summary>
		public void Update(object sender, EventArgs eventArgs)
		{
			var gameWindow = sender as OpenTK.GameWindow;

			if (gameWindow != null && gameLevelInput.CommandSwitchScreenMode.IsRising)
			{
				gameWindow.WindowState = gameWindow.WindowState == WindowState.Fullscreen ? WindowState.Normal : WindowState.Fullscreen;
			}

			frameStopwatch.Stop();
			var deltaTime = frameStopwatch.Elapsed;

			frameStopwatch.Restart();

			gameLevelInput.Update(deltaTime);
			currentLevel.Update(deltaTime);
		}

		/// <summary>
		/// Invoke every time game needs to render frame
		/// </summary>
		public void Render(object sender, EventArgs eventArgs)
		{
			var gameWindow = sender as OpenTK.GameWindow;
			if (gameWindow == null)
				throw new ArgumentException("Sender needs to be of type GameWidnow.");

			currentLevel.Render(gameWindow);

			gameWindow.SwapBuffers();
		}

		#endregion Methods
	}
}
